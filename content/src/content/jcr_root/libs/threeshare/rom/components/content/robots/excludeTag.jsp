<%@include file="/libs/foundation/global.jsp"%><%@page
import="com.threeshare.rom.search.SearchUtil"%><%@page
import="java.util.*"%><%

response.setContentType("text/plain");


//TODO: Use resourceResolver.map to clean full paths
List<String> removePaths = new ArrayList<String>();
removePaths.add("/content/twc");
removePaths.add("/content/twc/en/business-home");

//TODO: Pass tags in via query string
List<String> tagsList  = new ArrayList<String>();
tagsList.add("residential:nosearch");
tagsList.add("businessclass:nosearch");

List<Node> results = SearchUtil.getTaggedResources(tagsList,"/content",resourceResolver);

if (results!=null)
{
    for (Node node : results)
	{
        String nodePath = node.getPath(); 

        if ("cq:Page".equals(node.getProperty("jcr:primaryType").getString()))
        {
			%>Disallow: <%= node.getPath() %>.html
<%
            for (String removePath : removePaths)
            {
				if (nodePath.startsWith(removePath))
                    {
                        String cleanPagePath = nodePath.replace(removePath, "");
                        if (cleanPagePath.length() > 1)
                        {
                            %><%="Disallow: " + cleanPagePath + ".html"%>
<%  
                        }
                    }
            }
        }
        else if ("dam:Asset".equals(node.getProperty("jcr:primaryType").getString()))
        {
			%>Disallow: <%= node.getPath() %>
<%
        }
    }
}
%>