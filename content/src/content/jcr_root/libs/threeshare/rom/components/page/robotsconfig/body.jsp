<%@include file="/libs/foundation/global.jsp" %>

<%--
  ~ Copyright 2014 3|SHARE Corporation
  ~ All Rights Reserved.
--%>

<body>
	<div class="centerContainer">
	    <h1>Robots Configuration</h1>

    	<cq:include path="robots" resourceType="/libs/threeshare/rom/components/content/robotsGenerator"/>

    </div>
</body>