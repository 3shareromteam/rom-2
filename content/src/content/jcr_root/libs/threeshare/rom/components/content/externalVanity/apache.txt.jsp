<%--
  Copyright 2012 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

  ==============================================================================
--%><%@include file="/libs/foundation/global.jsp"%><%

response.setContentType("text/plain");    
		
// Define option to enable external vanity page
boolean useExtVanity = "true".equals(request.getParameter("external"));
boolean externalOnly = "only".equals(request.getParameter("external"));

// Define option to allow external vanities first
boolean priority = "external".equals(request.getParameter("priority"));

if (externalOnly)
{
	%><cq:include script="external.apache.txt.jsp" /><%
}
else if (useExtVanity && priority)
{
	%><cq:include script="external.apache.txt.jsp" /><%
	%><cq:include script="standard.apache.txt.jsp" /><%
}
else
{
	%><cq:include script="standard.apache.txt.jsp" /><%
	%><cq:include script="external.apache.txt.jsp" /><%
	
}
%>
