<%--
  Copyright 2012 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

  ==============================================================================
--%><%@include file="/libs/foundation/global.jsp"%><%@page import=
    "com.threeshare.rom.config.ROMConfigManager,
    com.threeshare.rom.util.ROMConstants,
	com.threeshare.rom.util.ROMStringUtils,
    com.threeshare.rom.util.RequestUtils,
    com.threeshare.rom.vanity.VanityFactory,
    com.threeshare.rom.vanity.VanityConfigNode,
    com.threeshare.rom.vanity.VanityNode,
    java.util.*"%><%

//Set Default Content Path
String content = "/content";

// Set content from contentPath property on Node
if (currentNode.hasProperty("contentPath"))
{
   content = currentNode.getProperty("contentPath").getString();
}

// Set content from query parameter 'content'
String contentOverride = request.getParameter("content");
if (contentOverride != null && !contentOverride.equals(""))
{
    content = contentOverride;
}

// Set Rewrite Parameters
String defaultRewriteParam = "";
String rewriteParam = request.getParameter("extParam");
if (rewriteParam == null || rewriteParam.equals(""))
{
    rewriteParam = defaultRewriteParam;
}

// DEPRECATED - Set Remove Path, used for removing content path to match Resource Resolver rules
String defaultRemovePath = "";
String removePath = request.getParameter("removePath");
if (removePath == null || removePath.equals(""))
{
    removePath = defaultRemovePath;
}

// Get optional trailing extensions
String[] defaultExtensions = {"/"};
String[] extensions = RequestUtils.getParameterArray(request,"extensions");
if (extensions == null || extensions.length < 0)
{
	extensions = defaultExtensions;
}


// Set response type
response.setContentType("text/plain");

// Configurations
String headerText = currentPage.getTitle() == null ? xssAPI.encodeForHTML(currentPage.getName()) : xssAPI.encodeForHTML(currentPage.getTitle());
String configPath = currentPage.getPath() + "/jcr:content/redirects";
String contentPath = "";

if (currentNode.hasProperty("extVanityTitle"))
{
    headerText = currentNode.getProperty("extVanityTitle").getString();
}
if (currentNode.hasProperty("configNode"))
{
    configPath = currentNode.getProperty("configNode").getString();
}
if (currentNode.hasProperty("contentPath"))
{
    contentPath = currentNode.getProperty("contentPath").getString();
}
String defaultRedirectType = "302";

// Get Vanity configuration node
VanityConfigNode configNode = VanityFactory.getConfigNode(resourceResolver, configPath);
boolean invalidConfig = configNode == null;

    %># Redirect Manager auto-generated rewrites

<%

// Get Vanities List from ConfigNode
List<VanityNode> vanities = invalidConfig ? new ArrayList<VanityNode>() : configNode.getVanities();

for (VanityNode vanityObj : vanities)
{
    String vanity = vanityObj.getVanityURI();
    String target = vanityObj.getVanityTarget();
    String redirectType = vanityObj.getRedirectType().equals("")
        ? defaultRedirectType : vanityObj.getRedirectType();

    if (vanity.length() > 0 && vanity != null)
    {
        vanity = vanity.trim();

        if (!vanity.startsWith("/") && !vanity.startsWith("http"))
        {
            vanity = "/" + vanity;
        }

        if (vanity.endsWith("/"))
        {
            vanity = vanity.substring(0, vanity.length()-1);
        }

        if (!removePath.equals(defaultRemovePath))
        {
            target = target.replace(removePath,"");
        }

        // Escape % for apache rewrites
        target = target.replace("%","\\%");
        vanity = vanity.replace("%","\\%");
        target = target.replace(" ","\\ ");
        vanity = vanity.replace(" ","\\ ");
        target = target.replace("(","\\(");
        vanity = vanity.replace("(","\\(");
        target = target.replace(")","\\)");
        vanity = vanity.replace(")","\\)");

        // Add domain handling rewrite condition
        if (vanity.startsWith("http://") || vanity.startsWith("https://"))
        {
            String domain = ROMStringUtils.getDomainName(vanity);

		  	    if ( domain != null && domain.length() > 0)
            {
				        int indexOfDomain = vanity.indexOf(domain);
                int index = indexOfDomain+domain.length();

                // if vanity is just a domain (i.e. http://domain) add a trailing /
				        if (index == vanity.length())
                {
                    vanity += "/";
                }

                vanity = vanity.substring(index);

				        %>RewriteCond %{HTTP_HOST} ^<%=domain%>$ [NC] #
<%

            }

        }

        if (vanity.contains("?"))
        {
            int queryStart = vanity.indexOf("?");
			      String vanityURL = vanity.substring(0,queryStart);
            String queryParamsAll = vanity.substring(queryStart + 1, vanity.length());
			      String[] queryParams = queryParamsAll.split("&");
            for (String queryParam : queryParams)
            {

              if (queryParam.length() > 0 || !queryParam.equals(""))
              {
                  %>RewriteCond %{QUERY_STRING} <%=queryParam%> [NC] #
<%
              }
            }

            vanity = vanityURL;
        }

        StringBuilder sb = new StringBuilder();
        int extensionCount = 0;
        String extensionDelimiter = "";
        sb.append("(");
        for (String extension: extensions)
    	{
            if (extension != null && extension.length() > 0)
            {
                if (extension.equals("disabled"))
                {
					break;
                }
                if (extensionCount > 0)
                {
					extensionDelimiter = "|";
                }
                sb.append(extensionDelimiter);
                sb.append(extension);

                extensionCount ++;
            }
        }
        sb.append(")?");
        String extensionFinal = sb.toString();

		%>RewriteRule ^<%=vanity %><%=extensionFinal%>$ <%=target %> [R=<%=redirectType%><%=rewriteParam %>] #

<%
    }

}


%>
