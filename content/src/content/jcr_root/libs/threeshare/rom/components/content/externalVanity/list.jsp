<%--
  Copyright 2013 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

  ==============================================================================
--%>
<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.threeshare.rom.config.ROMConfigManager" %>
<%@ page import="com.threeshare.rom.util.ROMConstants" %>
<%@ page import="com.threeshare.rom.util.RequestUtils" %>
<%@ page import="org.apache.sling.api.request.RequestPathInfo" %>
<%@ page import="com.threeshare.rom.vanity.VanityFactory"%>
<%@ page import="com.threeshare.rom.vanity.VanityConfigNode"%>
<%@ page import="com.threeshare.rom.vanity.VanityNode"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collections"%>

<%

String configPath = "";

if (currentNode.hasProperty("configNode"))
{
    configPath = currentNode.getProperty("configNode").getString(); 
}

// Get Vanity configuration node
VanityConfigNode configNode = VanityFactory.getConfigNode(resourceResolver, configPath);
boolean invalidConfig = configNode == null;
// Get Vanities List from ConfigNode
List<VanityNode> vanities = invalidConfig ? new ArrayList<VanityNode>() : configNode.getVanities();

Collections.sort(vanities, VanityNode.vanityURIComparatorAsc);


%>

<table>
  <tr>
    <td>Vanity</td>
    <td>Target</td>
    <td>Type</td>  
  </tr>
<%

    for (VanityNode vanity : vanities)
    {
        String vanityURI = vanity.getVanityURI();
        String vanityTarget = vanity.getVanityTarget();
        String redirectType = vanity.getRedirectType();

        if (vanityURI.length() > 0 && vanityURI != null)
        {%>
              <tr>
                <td><%=vanityURI%></td>
                <td><%=vanityTarget%></td>
                <td><%=redirectType%></td>
              </tr>
		<%

        }
    }
%>