<%@include file="/libs/foundation/global.jsp"%><%@page
import="com.threeshare.rom.search.SearchUtil"%><%@page
import="java.util.List"%><%
response.setContentType("text/plain");
String[] excludedTags = properties.get("excludeTags",String[].class);

if (excludedTags != null && excludedTags.length > 0)
{
    %>
# Excluded Tags
<%

    List<Node> results = SearchUtil.getTaggedResources(excludedTags,"/content",resourceResolver);
    if (results!=null)
    {
        for (Node node : results)
        {
            if ("cq:Page".equals(node.getProperty("jcr:primaryType").getString()))
            {
                %>Disallow: <%= resourceResolver.map(node.getPath()) %>.html #
<%
            }
            else if ("dam:Asset".equals(node.getProperty("jcr:primaryType").getString()))
            {
                %>Disallow: <%= resourceResolver.map(node.getPath()) %> #
<%
            }
        }
    }
}
%>
