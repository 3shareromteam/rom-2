<%@include file="/libs/foundation/global.jsp"%><%

String crawlDelay = properties.get("crawlDelay","10");
String sitemapPath = properties.get("sitemapPath","");
String userAgent = properties.get("userAgent","*");

response.setContentType("text/plain");

if (sitemapPath != null && sitemapPath.length() > 0)
{
	%>Sitemap: <%=sitemapPath%>
<%}%>
User-agent: <%=userAgent%>

<%
if (crawlDelay != null && !crawlDelay.equals("0"))
{
	%>Crawl-delay: <%=crawlDelay%>
<%}%>

<cq:include script="allowPaths.jsp"/><%

%><cq:include script="excludePages.jsp"/><%

%><cq:include script="excludeTags.jsp"/><%

%><cq:include script="excludePaths.jsp"/><%

%>
