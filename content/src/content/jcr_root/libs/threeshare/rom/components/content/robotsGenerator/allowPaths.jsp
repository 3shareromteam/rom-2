<%@include file="/libs/foundation/global.jsp"%><%@page
import="com.threeshare.rom.search.SearchUtil"%><%@page
import="java.util.List"%><%
response.setContentType("text/plain");
String[] allowPaths = properties.get("allowPaths",String[].class);

if (allowPaths != null && allowPaths.length > 0)
    {
        %>

# Allowed Paths
<%
        for (String allowPath : allowPaths)
        {
    		    if (allowPath != null && allowPath.length() > 0)
            {
            %>Allow: <%=resourceResolver.map(allowPath)%> #
<%
            }
        }
    }
%>
