<%@include file="/libs/foundation/global.jsp"%><%
%><%@ page import="com.threeshare.rom.replication.ROMReplication,
                 com.threeshare.rom.replication.impl.ROMReplicationImpl"%><%
%><%
    ROMReplication romReplication = sling.getService(ROMReplication.class);
    String include = null;
    String exclude = null;
    String includeParam = request.getParameter("include");
    if (includeParam != null && !includeParam.equals(""))
    {
	    include = includeParam;
    }
    String excludeParam = request.getParameter("exclude");
    if (excludeParam != null && !excludeParam.equals(""))
    {
        exclude = excludeParam;
    }

    String data = romReplication.getReplicationQueueSizes(exclude,include);
    response.setContentType("application/xml");
%><%= data %>