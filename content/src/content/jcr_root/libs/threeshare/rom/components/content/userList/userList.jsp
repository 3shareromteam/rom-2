<%--
  Copyright 2012 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

  ==============================================================================
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.threeshare.rom.search.SearchUtil" %>
<%@ page import="java.util.*" %>
<%@ page import="org.apache.sling.api.resource.Resource" %>
<%@ page import="org.apache.jackrabbit.api.security.user.*"%>
<%@ page import="com.threeshare.rom.util.RequestUtils" %>
<%

    Session session = resourceResolver.adaptTo(Session.class);
    boolean saveSession = "true".equals(request.getParameter("save"));
	String defaultGroupDelimiter = "<br>";
	String groupDelimiter = RequestUtils.getParameter(request,"groupDelimiter",defaultGroupDelimiter);
    String defaultSearchPath = "/home/users";
    String type = "rep:User";
    String searchPath = request.getParameter("save");
    if (searchPath == null || searchPath.length() < 1)
    {
        searchPath = defaultSearchPath;
    }
    List<Resource> searchResources = SearchUtil.searchByType(searchPath, resourceResolver, type);

%>
    <style>
      tr:nth-child(even) {background-color: #f2f2f2}
      table { width: 100%; border-collapse: collapse; }
      th { height: 50px; }
      table, th, td { border: 1px solid black; }
      th { height: 30px; }
    </style>

    <table>
    <tr>
        <th>User ID</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Home Path</th>
        <th>Groups</th>
    </tr>
<%
    for (Resource searchResource: searchResources)
    {
		    if (searchResource != null)
        {
            Authorizable user = searchResource.adaptTo(User.class); // org.apache.jackrabbit.api.security.user.Authorizable
            if (user != null)
            {
                String firstName = null;
                if (user.hasProperty("profile/givenName"))
                {
                	Value firstNameVal = user.getProperty("profile/givenName")[0];

                    firstName = firstNameVal != null ? firstNameVal.toString() : null;
                }
                String lastName = null;
                if (user.hasProperty("profile/familyName"))
                {
                	Value lastNameVal = user.getProperty("profile/familyName")[0];
                    lastName = lastNameVal != null ? lastNameVal.toString() : null;
                }
				String email = null;
                if (user.hasProperty("profile/email"))
                {
                	Value emailVal = user.getProperty("profile/email")[0];
                    if (emailVal != null)
                    {
                    	email = emailVal.toString();
                    }
                }
                StringBuilder sb = new StringBuilder();
                Iterator<Group> currentUserGroups = user.memberOf();

                while(currentUserGroups.hasNext())
                {
                    Group group = currentUserGroups.next();
                    String groupName = "";
					if (group.hasProperty("profile/givenName"))
                	{
                        Value groupNameVal = group.getProperty("profile/givenName")[0];
                        if (groupNameVal != null)
                        {
                            groupName = groupNameVal.toString();
                            if (groupName.equals(""))
                            {
                                groupName = group.getID();
                            }
                        }
                        sb.append(groupName + groupDelimiter);
                    }
                }
                String groups = sb.toString();

                %>
            		<tr>
    			      <th><%=user.getID()%></th>
    			      <th><%=firstName != null ? firstName : ""%></th>
    			      <th><%=lastName != null ? lastName : ""%></th>
    			      <th><%=email != null ? email : ""%></th>
    			      <th><%=user.getPath()%></th>
    			      <th><%=groups != null ? groups : ""%></th>
            		</tr>
        		    <%
            }
        }
    }
%>

</table>
