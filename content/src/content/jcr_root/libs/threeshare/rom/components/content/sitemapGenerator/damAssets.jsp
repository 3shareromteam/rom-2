<%@include file="/libs/foundation/global.jsp"%><%
%><%@ page import="com.threeshare.rom.search.SearchUtil,
    				com.threeshare.rom.util.ROMStringUtils,
					com.day.cq.wcm.api.PageManager,
					com.day.cq.tagging.TagManager,
					com.day.cq.tagging.Tag,
					com.day.cq.dam.api.Asset,
                    java.util.List,
                    java.util.ArrayList,
                    java.util.Arrays,
                    java.util.Calendar,
                    java.text.SimpleDateFormat,
                    java.util.Date"%><%    


//##########################################################################
// Initialization
//########################################################################## 

response.setContentType("application/xml");

String siteDomain = properties.get("sitemapconfig/siteDomain","");
String pageExtension = properties.get("sitemapconfig/pageExtension","");
String rootPagePath = properties.get("sitemapconfig/rootPagePath","");
String changeFrequency = properties.get("sitemapconfig/changeFrequency","");
String linkPriority = "0.5";

String[] damSearchPaths = properties.get("sitemapconfig/damSearchPaths",String[].class);   
String[] excludePaths = properties.get("sitemapconfig/excludePaths",String[].class);
String[] excludeTags = properties.get("sitemapconfig/excludeTags",String[].class);

PageManager pm = slingRequest.getResourceResolver().adaptTo(PageManager.class);

SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

List<String> damAssetPaths = SearchUtil.getDAMAssets(damSearchPaths, resourceResolver);
damAssetPaths = ROMStringUtils.removeDuplicates(damAssetPaths);

TagManager tagManager = resourceResolver.adaptTo(TagManager.class);

for (String damAssetPath : damAssetPaths)
{
    Resource damResource = resourceResolver.getResource(damAssetPath + "/jcr:content/metadata");
	ValueMap damProperties = damResource.adaptTo(ValueMap.class);

    //Exclusions
    boolean ignore = false;
    
    // Check for tag exclusions
	String[] damTags = damProperties.get("cq:tags", String[].class);
    if (damTags != null && excludeTags != null)
    {
        for (String damTag : damTags)
        {
            if (ignore)
            {
                break;
            }
            else
            {
                for (String excludeTag : excludeTags)
                {
                    if (ignore)
                    {
                        break;
                    }
                    else
                    {
                        if (excludeTag.equals(damTag))
                        {
                            // Exclusion found;
                            ignore = true;
                        }
                    }
                }
            }
        }
    }


    // Check for path exclusions
    if (excludePaths != null && !ignore)
    {
        for(String excludePath : excludePaths)
        {
            if (ignore)
            {
                break;
            }
            else
            {
                if (damAssetPath.contains(excludePath))
                {
                    // Exclusion found; 
                    ignore = true;
                }
            }	
        }
    }    


    if (!ignore)
    {

		String cleanedAssetPath = resourceResolver.map(damAssetPath);
        String assetPath = siteDomain + cleanedAssetPath;
        
        %>
<url>
    <loc><%=assetPath%></loc>
    <changefreq><%=changeFrequency %></changefreq>
    <priority><%=linkPriority%></priority>
</url><%

    }

}
%>
