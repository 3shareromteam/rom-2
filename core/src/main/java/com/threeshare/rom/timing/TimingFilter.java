/*************************************************************************
*
* ADOBE CONFIDENTIAL
* ___________________
*
*  Copyright 2011 Adobe Systems Incorporated
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Adobe Systems Incorporated and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Adobe Systems Incorporated and its
* suppliers and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Adobe Systems Incorporated.
**************************************************************************/
package com.threeshare.rom.timing;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.threeshare.rom.timing.TimingUtils;

/**
 * @scr.component metatype="no"
 * @scr.property name="service.description" value="3|SHARE Timing Request Filter"
 * @scr.property name="service.vendor" value="3|SHARE"
 * @scr.property name="service.ranking" value="2147483647"
 * @scr.property name="filter.scope" value="INCLUDE" private="true"
 * @scr.service
 */

public class TimingFilter implements Filter
{
	private static final Logger log = LoggerFactory.getLogger(TimingFilter.class);
	private String parameter = "timing";
	private String enableParam = "true"; 
	
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
		FilterChain chain) throws IOException, ServletException
	{
		try
		{
			if (request instanceof SlingHttpServletRequest)
			{
				
				SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) request;
				String parameterValue = slingRequest.getParameter(parameter);
				if (parameterValue != null && parameterValue.length() > 0)
			    {
					if (parameterValue.equals(enableParam))
					{
						String requestType = "json";
						String timingData = TimingUtils.getComponentTiming(request, requestType);
						log.info(timingData);
					}
					else
					{
						log.debug("Parameter: " + parameter + " does not equal " + enableParam);
					}
			    }
				else
				{
					log.debug("No valid parameter (" + parameter + ") found.");
					
				}
			}
		}
		catch (Exception e)
		{
			log.error("Timing Filter Error. " + e);
		}
		finally
		{
			chain.doFilter(request, response);
		}
		
	}

	@Override
	public void destroy() {		
	}
	
}