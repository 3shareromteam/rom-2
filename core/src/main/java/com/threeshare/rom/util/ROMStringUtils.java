package com.threeshare.rom.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ROMStringUtils 
{
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ROMStringUtils.class);
	
	public static String cleanPath(String path)
	{
		// Default behavior for cleanPath. 
		// Add leading "/", remove trailing "/", remove "//"
		boolean addLeadingSlash = false;
		boolean addTrailingSlash = true;
		boolean removeDoubleSlash = true;
		return cleanPath(path, addLeadingSlash, addTrailingSlash, removeDoubleSlash);
	}
	
	public static String cleanPath(String path, boolean addLeadingSlash, boolean addTrailingSlash, boolean removeDoubleSlash)
	{
		String slashChar = "/";
		
		if ("/".equals(path))
		{
			return "/";
		}
		
		if (path != null && path.length() > 0)
		{
			path = path.trim();
			
			if (removeDoubleSlash)
			{
				path = removeDoubleSlashFromPath(path);
			}
			
			if (path.startsWith(slashChar))
			{
				path = path.substring(1);
			}
			
			if (path.endsWith(slashChar))
	        {
				path = path.substring(0, path.length()-1);
	        }
			
		}
		else
		{
			return null;
		}
		
		if (path.length() > 0)
		{
			if (addLeadingSlash)
			{
				path = slashChar + path;
			}
			if (addTrailingSlash)
			{
				path = path + slashChar;
			}
		}
		else
		{
			if (addLeadingSlash)
			{
				path = slashChar + path;
			}
			else
			{
				return null;
			}
		}
		
		return path;
	}
	
	public static String removeDoubleSlashFromPath(String path)
	{
		String slashChar = "/";
		
		if(path.contains(slashChar+slashChar))
		{
			path = path.replace(slashChar+slashChar, slashChar);
			
			// Add recursion to check for more than two slashes
			path = removeDoubleSlashFromPath(path);
		}
		
		return path;
	}

	public static String[] removeDuplicates (String[] array)
	{
		// Convert array to list
		List<String> list = Arrays.asList(array);
		list = removeDuplicates(list);
		String[] cleanArray = list.toArray(new String[list.size()]);
		
		return cleanArray;
		
	}
	
	public static List<String> removeDuplicates (List<String> list)
	{
		// Convert to set to remove duplicates
		Set<String> set = new HashSet<String>();
		set.addAll(list);
		List<String> cleanList = new ArrayList<String>();
		cleanList.addAll(set);
		
		return cleanList;
	}
	
	public static boolean isInteger(String str) {
		if (str == null) {
			return false;
		}
		int length = str.length();
		if (length == 0) {
			return false;
		}
		int i = 0;
		if (str.charAt(0) == '-') {
			if (length == 1) {
				return false;
			}
			i = 1;
		}
		for (; i < length; i++) {
			char c = str.charAt(i);
			if (c <= '/' || c >= ':') {
				return false;
			}
		}
		return true;
	}
	
	public static String getDomainName(String url) {
		try 
		{
		    if(!url.startsWith("http") && !url.startsWith("https"))
		    {
		         url = "http://" + url;
		    }        
		    URL netUrl = new URL(url);
		    String host = netUrl.getHost();
		    return host;
		} 
		catch (Exception e)
		{
			log.error(e.toString(), e);
		}
		finally
		{
			
		}
		
		return null;
				
	}
	
	public static boolean isNotBlank(String text) {
		try 
		{
			if (text == null)
			{
				return false;
			}
			if (text.trim().length() < 1)
			{
				return false;
			}
		} 
		catch (Exception e)
		{
			log.error(e.toString(), e);
		}
		finally
		{
			
		}
		
		return true;
				
	}	
	
}