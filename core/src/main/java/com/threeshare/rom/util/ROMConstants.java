/*************************************************************************

  Copyright 2013 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

**************************************************************************/

package com.threeshare.rom.util;

public class ROMConstants {

	public static long DEFAULT_GET_SERVLET_BUNDLE_ID = 150;
	public static final String ETC_NODE_PATH = "/etc/";
	public static final String CONFIG_NODE_NAME = "config";
	public static final String ROM_CONFIG_NODE_NAME = "threeshare";
	
	// Node Types
	public static final String NODE_TYPE_PROPERTY = "nodeType";
	public static final String VANITY_CONFIG_NODE_TYPE = "vanityConfig";
	public static final String VANITY_NODE_TYPE = "vanity";
	
	// Vanity Types
	public static final String VANITY_TYPE_PROPERTY = "vanityType";
	public static final String VANITY_TYPE_CUSTOM = "custom";
	public static final String VANITY_TYPE_CUSTOM_JCR_PROPERTY = "sling:vanityPathCustom";
	public static final String VANITY_TYPE_STANDARD = "standard";
	public static final String VANITY_TYPE_STANDARD_JCR_PROPERTY = "sling:vanityPath";
	
	// Vanity Page Properties
	public static final String VANITY_URI_PAGE_PROPERTY_KEY = "vanity";
	public static final String REDIRECT_TYPE_PAGE_PROPERTY_KEY = "redirectType";
	
	// Vanity Configuration
	public static final String VANITY_DELIMITER = "<vanity>";
	public static final String REDIRECT_DELIMITER = "<redirect>";
	public static final String NEW_VANITY_TAG = "newVanity";	
	public static final String VANITY_URL_PROPERTY = "vanity";
	public static final String TARGET_URL_PROPERTY = "target";
	public static final String REDIRECT_TYPE_PROPERTY = "redirectType";
	public static final String CONFIG_NODE_PATH_PROPERTY = "configNode";
	public static final String CONFIG_NODE_VANITY_DOMAIN_PROPERTY = "vanityDomain";
	public static final String CONFIG_NODE_EXCLUSIONS_PROPERTY = "exclusions";
	public static final String DEFAULT_CONFIG_NODE_PATH_VALUE = "/content";
	public static final String DEFAULT_REDIRECT_TYPE = "301";
	public static final String CONFIG_NODE_PROPERTY = "configType";
	public static final String VANITY_NODE_ID_PROPERTY = "nodeType";

	public static final String VANITY_NODE_PREFIX = "vanity_";
	
	// Vanity Servlet
	public static final String REDIRECT_TYPE_JSON_KEY = "redirectType";
	public static final String VANITY_URI_JSON_KEY = "vanityURI";
	public static final String TARGET_JSON_KEY = "target";
	public static final String NODE_PATH_JSON_KEY = "nodePath";
	public static final String POST_JSON_VANITY_PARAM = "vanity";
	public static final String POST_JSON_CONFIG_PARAM = "configNode";
	public static final String REQUEST_TYPE_PARAM = "requestType";
	public static final String UPDATE_VANITY_REQUEST_TYPE = "update";
	public static final String DELETE_VANITY_REQUEST_TYPE = "remove";
	public static final String JSON_OBJECT_CONFIG_ID = "configNode";
	public static final String JSON_OBJECT_VANITY_ID = "vanityPost";
	public static final String VANITY_CHECK_VANITY_PARAM = "vanityURI";
	public static final String VANITY_CHECK_PATH_PARAM = "path";
	public static final String VANITY_CHECK_REQ_TYPE_PARAM = "vanityCheck";
	
	// General Servlet Constants
	public static final String JSON_EXTENSION = ".json";
	public static final String XML_EXTENSION = ".xml";
	public static final String PARAMETER_DELIMITER = ",";
	public static final String ANONYMOUS_USER = "anonymous";
	public static final String ROOT_CONTENT_PATH = "/content";

	// Timing Constants
	public static final String JSON_REQUEST_TYPE = "json";
	public static final String HTML_REQUEST_TYPE = "html";
}
