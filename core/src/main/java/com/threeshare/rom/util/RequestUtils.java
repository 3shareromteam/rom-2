/*************************************************************************

  Copyright 2013 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

**************************************************************************/

package com.threeshare.rom.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.threeshare.rom.timing.TimingUtils;

public class RequestUtils
{
	private static final Logger log = LoggerFactory.getLogger(RequestUtils.class);
	
	public static String[] getParameterArray(HttpServletRequest request, String parameter)
	{
		String delimiter = ROMConstants.PARAMETER_DELIMITER;
		return getParameterArray(request, parameter, delimiter);
	}
	
	public static String[] getParameterArray(HttpServletRequest request, String parameter, String delimiter) {
		
		String parameterValue = request.getParameter(parameter);
		
	    if (parameterValue != null && parameterValue.length() > 0)
	    {
	    	String[] parameterArray = parameterValue.split(delimiter);
	    	return parameterArray;
	    }
	    else
	    {
	    	return null;
	    }
	}
	
	public static String getParameter(HttpServletRequest request, String parameter) 
	{
		return getParameter(request, parameter, null);
	}
	
	public static String getParameter(HttpServletRequest request, String parameter, String defaultValue) 
	{
		String parameterValue = request.getParameter(parameter);
		if (parameterValue == null || parameterValue.length() < 1)
	    {
			return defaultValue;
	    }
		
		return parameterValue;
	}
	
	public static String getExtension(HttpServletRequest request) 
	{
		URL url;
		String extension = "";
		
		try 
		{
			url = new URL(request.getRequestURL().toString());
			String file = url.getFile();
			if (file.contains("."))
			{
				extension = file.substring(file.lastIndexOf("."));
			}
		} 
		catch (MalformedURLException exception)
		{
			log.error("Malformed URL detected",exception);
			extension = "";
		}
		
		return extension;
	}
	
	public static String getRequestUser(HttpServletRequest request)
	{
		Principal principal = request.getUserPrincipal();
		String username = principal != null ? principal.getName() : null;
		
		return username;
	}
	
	public static String getTiming(HttpServletRequest request)
	{
		return getTiming(request, null);
	}
	
	public static String getTiming(HttpServletRequest request, String requestType)
	{
		return TimingUtils.getComponentTiming(request, requestType);
	}
}