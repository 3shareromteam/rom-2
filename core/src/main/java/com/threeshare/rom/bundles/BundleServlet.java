/*************************************************************************

  Copyright 2013 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

**************************************************************************/
package com.threeshare.rom.bundles;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Dictionary;

import javax.servlet.ServletException;
import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.osgi.PropertiesUtil;

import org.osgi.framework.Bundle;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.threeshare.rom.util.ROMConstants;
import com.threeshare.rom.util.RequestUtils;

@Component(metatype = true, immediate = true, label = "3|SHARE ROM Bundle Monitor", description = "ROM Monitoring service for bundles")
@Service(value = Servlet.class)
@Properties({
	@Property (name="service.vendor", value="3|SHARE ROM"),
	@Property (name="service.pid", value="com.threeshare.rom.bundles.BundleServlet")
})
	
public class BundleServlet extends SlingAllMethodsServlet
{
	private static final long serialVersionUID = -8113276270892439174L;
	private static final Logger log = LoggerFactory.getLogger(BundleServlet.class);
	private boolean monitorEnabled = true;
	
	// Defined Input Parameters
	public static final String arrayParameterDelimiter = ",";
	public static final String filterParameter = "filter";
	
	@Property (label = "Monitor Enabled", description = "Option to enable/disable the bundle monitoring. ", boolValue = true)
	protected static final String SERVLET_ENABLED = "servlet.monitor.enabled";
	
	@Property (label = "Servlet Paths", description = "Active servlet paths. All paths must be in the sling execution paths in order to respond.", value = { "/bin/threeshare/bundle_monitor" , "/bin/3share/bundle_monitor" })
	protected static final String SERVLET_SELECTORS = "sling.servlet.paths";
		
	@Activate
    protected void activate(ComponentContext context) throws Exception
    {
		// On activation, retrieve values from OSGI configuration.
		@SuppressWarnings("unchecked")
		final Dictionary<String, Object> properties = context.getProperties();
		String monitorEnabledValue = PropertiesUtil.toString(properties.get(SERVLET_ENABLED), "true").toLowerCase();
		monitorEnabled = monitorEnabledValue.equals("true");
	}
	
	protected void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException
	{
		String responseMessage = "test";
		int responseCode = 200;
		log.debug("Call made to ROM Bundle Monitor servlet. Servlet responded with \"" + responseMessage +  "\"");
		
		String extension = RequestUtils.getExtension(request);
		if (extension.equals(ROMConstants.JSON_EXTENSION))
		{
			//send to json response processor
			writeJsonResponse(response, responseMessage, responseCode);
		}
		else if (extension.equals(ROMConstants.XML_EXTENSION))
		{
			//send to xml processor
			writeXmlResponse(response, responseMessage, responseCode);
		}
		else
		{
			//use normal html processing.
			writeHtmlResponse(response, responseMessage, responseCode);
		}
		
	}

	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException
	{
		String responseMessage = processBundlesRequest(request);
		int responseCode = 200;
		log.debug("Call made to ROM Bundle Monitor servlet. Servlet responded with \"" + responseMessage +  "\"");
		
		String extension = RequestUtils.getExtension(request);
		if (extension.equals(ROMConstants.JSON_EXTENSION))
		{
			//send to json response processor
			writeJsonResponse(response, responseMessage, responseCode);
		}
		else if (extension.equals(ROMConstants.XML_EXTENSION))
		{
			//send to xml processor
			writeXmlResponse(response, responseMessage, responseCode);
		}
		else
		{
			//use normal html processing.
			writeHtmlResponse(response, responseMessage, responseCode);
		}
	}

	private String processBundlesRequest(SlingHttpServletRequest request) {
		
		String[] filters = RequestUtils.getParameterArray(request, filterParameter);
		Bundle[] filteredBundles = BundleUtils.getFilteredBundles(filters);
		
		String extension = RequestUtils.getExtension(request);
		String message = "";
		
		if (extension.equals(ROMConstants.JSON_EXTENSION))
		{
			//send to json processor
			message = BundleMonitor.processJsonBundleRequest(filteredBundles);
		}
		else if (extension.equals(ROMConstants.XML_EXTENSION))
		{
			//send to xml processor
			message = BundleMonitor.processXmlBundleRequest(filteredBundles);
		}
		else
		{
			//use normal html processing.
			message = BundleMonitor.processHtmlBundleRequest(filteredBundles);
		}
		
		return message;
	}

	private static void writeHtmlResponse(SlingHttpServletResponse response, String msg, int code) throws IOException
	{
		String contentType = "text/html;charset=UTF-8";
		writeResponse(response, msg, code, contentType);
	}
	
	private static void writeJsonResponse(SlingHttpServletResponse response, String msg, int code) throws IOException
	{
		String contentType = "application/json;charset=UTF-8";
		writeResponse(response, msg, code, contentType);
	}
	
	private static void writeXmlResponse(SlingHttpServletResponse response, String msg, int code) throws IOException
	{
		String contentType = "application/xml;charset=UTF-8";
		writeResponse(response, msg, code, contentType);
	}
	
	private static void writeResponse(SlingHttpServletResponse response, String msg, int code, String contentType)  throws IOException 
	{
		response.setStatus(code);
		response.setContentType(contentType);
		PrintWriter out = response.getWriter();
		out.println(msg);
		out.close();		
	}
}