package com.threeshare.rom.system.data;

public class OSData {
	private long openFileDescriptorCount;
	private long maxFileDescriptorCount;
	private long totalPhysicalMemorySize;
	private long freePhyscialMemorySize;
	private int availableProcessors;
	private double systemLoadAverage;
	
	public void setOpenFileDescriptorCount(long count) {
		this.openFileDescriptorCount = count;
	}
	
	public void setMaxFileDescriptorCount(long count) {
		this.maxFileDescriptorCount = count;
	}
	
	public void setTotalPhysicalMemorySize(long size) {
		this.totalPhysicalMemorySize = size;
	}
	
	public void setFreePhysicalMemorySize(long size) {
		this.freePhyscialMemorySize = size;
	}
	
	public void setAvailableProcessors(int count) {
		this.availableProcessors = count;
	}
	
	public void setSystemLoadAverage(double average) {
		this.systemLoadAverage = average;
	}
	
	public long getAvailableFileDescriptorCount() {
		return this.maxFileDescriptorCount - this.openFileDescriptorCount;
	}
	
	public long getTotalPhysicalMemorySize() {
		return this.totalPhysicalMemorySize;
	}
	
	public long getFreePhysicalMemorySize() {
		return this.freePhyscialMemorySize;
	}
	
	public long getAvailableProcessors() {
		return this.availableProcessors;
	}
	
	public double getSystemLoadAverage() {
		return this.systemLoadAverage;
	}
	
	public String toXML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<prtg>");
		sb.append("<result>");
		sb.append("<channel>Available File Descriptors</channel>");
		sb.append("<unit>Count</unit>");
		sb.append("<value>" + getAvailableFileDescriptorCount() + "</value>");
		sb.append("</result>");
		sb.append("<result>");
		sb.append("<channel>Total Physical Memory</channel>");
		sb.append("<unit>BytesMemory</unit>");
		sb.append("<volumeSize>Byte</volumeSize>");
		sb.append("<value>" + this.totalPhysicalMemorySize + "</value>");
		sb.append("</result>");
		sb.append("<result>");
		sb.append("<channel>Free Physical Memory</channel>");
		sb.append("<unit>BytesMemory</unit>");
		sb.append("<volumeSize>Byte</volumeSize>");
		sb.append("<value>" + this.freePhyscialMemorySize + "</value>");
		sb.append("</result>");
		sb.append("<result>");
		sb.append("<channel>Available Processors</channel>");
		sb.append("<unit>Count</unit>");
		sb.append("<value>" + this.availableProcessors + "</value>");
		sb.append("</result>");
		sb.append("<result>");
		sb.append("<channel>System Load Average</channel>");
		sb.append("<unit>Count</unit>");
		sb.append("<value>" + this.systemLoadAverage + "</value>");
		sb.append("</result>");
		sb.append("</prtg>");
		return sb.toString();		
	}
}
