package com.threeshare.rom.system.impl;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.ThreadMXBean;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.threeshare.rom.system.data.DiskUsageData;
import com.threeshare.rom.system.data.JVMData;
import com.threeshare.rom.system.data.OSData;
import com.threeshare.rom.system.data.SlingData;
import com.threeshare.rom.system.data.ThreadsData;
import com.threeshare.rom.system.ROMSystem;

@Component
@Service
@Properties({
    @Property(name="service.description", value="ROM System Data Collector"),
    @Property(name="service.vendor", value="3|SHARE")
})
public class ROMSystemImpl implements ROMSystem {
	@Reference
	private MBeanServer mbeanServer;
		
    private final Logger log = LoggerFactory.getLogger(getClass());

	public String getJVMData() throws Exception {
		MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
		JVMData jvmData = new JVMData();
		jvmData.setTotalHeapMemory(memoryMXBean.getHeapMemoryUsage().getCommitted());
		jvmData.setUsedHeapMemory(memoryMXBean.getHeapMemoryUsage().getUsed());
		jvmData.setTotalNonHeapMemory(memoryMXBean.getNonHeapMemoryUsage().getCommitted());
		jvmData.setUsedNonHeapMemory(memoryMXBean.getNonHeapMemoryUsage().getUsed());		
		return jvmData.toXML();
	}

	public String getThreadData() throws Exception {
		ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
		ThreadsData threadData = new ThreadsData();
		threadData.setTotalThreadCount(threadMXBean.getThreadCount());
		return threadData.toXML();
	}
	
	public String getDiskUsageData() throws Exception {
		try {
			Long capacity = (Long) mbeanServer.getAttribute(new ObjectName("com.adobe.granite.monitoring:type=script,id=diskusage"), "disk.capacity");
			Long usage = (Long) mbeanServer.getAttribute(new ObjectName("com.adobe.granite.monitoring:type=script,id=diskusage"), "disk.usage");
			DiskUsageData diskUsageData = new DiskUsageData();
			diskUsageData.setCapacity(capacity);
			diskUsageData.setUsage(usage);
			return diskUsageData.toXML();
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	public String getOperatingSystemData() throws Exception {
		try {
			OSData osData = new OSData();
			osData.setOpenFileDescriptorCount((Long) mbeanServer.getAttribute(new ObjectName(ManagementFactory.OPERATING_SYSTEM_MXBEAN_NAME), "OpenFileDescriptorCount"));
			osData.setMaxFileDescriptorCount((Long) mbeanServer.getAttribute(new ObjectName(ManagementFactory.OPERATING_SYSTEM_MXBEAN_NAME), "MaxFileDescriptorCount"));
			osData.setTotalPhysicalMemorySize((Long) mbeanServer.getAttribute(new ObjectName(ManagementFactory.OPERATING_SYSTEM_MXBEAN_NAME), "TotalPhysicalMemorySize"));
			osData.setFreePhysicalMemorySize((Long) mbeanServer.getAttribute(new ObjectName(ManagementFactory.OPERATING_SYSTEM_MXBEAN_NAME), "FreePhysicalMemorySize"));
			osData.setAvailableProcessors((Integer) mbeanServer.getAttribute(new ObjectName(ManagementFactory.OPERATING_SYSTEM_MXBEAN_NAME), "AvailableProcessors"));
			osData.setSystemLoadAverage((Double) mbeanServer.getAttribute(new ObjectName(ManagementFactory.OPERATING_SYSTEM_MXBEAN_NAME), "SystemLoadAverage"));
			return osData.toXML();
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	public String getSlingData() throws Exception {
		try {
			SlingData slingData = new SlingData();
			slingData.setRequestCount((Long) mbeanServer.getAttribute(new ObjectName("org.apache.sling:type=engine,service=RequestProcessor"), "RequestsCount"));
			slingData.setMinRequestDurationMsec((Long) mbeanServer.getAttribute(new ObjectName("org.apache.sling:type=engine,service=RequestProcessor"), "MinRequestDurationMsec"));
			slingData.setMaxRequestDurationMsec((Long) mbeanServer.getAttribute(new ObjectName("org.apache.sling:type=engine,service=RequestProcessor"), "MaxRequestDurationMsec"));
			slingData.setStandardDeviationDurationMsec((Double) mbeanServer.getAttribute(new ObjectName("org.apache.sling:type=engine,service=RequestProcessor"), "StandardDeviationDurationMsec"));
			slingData.setMeanRequestDurationMsec((Double) mbeanServer.getAttribute(new ObjectName("org.apache.sling:type=engine,service=RequestProcessor"), "MeanRequestDurationMsec"));
			return slingData.toXML();
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}
}
