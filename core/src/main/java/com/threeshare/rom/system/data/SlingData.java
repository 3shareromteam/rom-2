package com.threeshare.rom.system.data;

public class SlingData {

	private long requestsCount;
	private long minRequestDurationMsec;
	private long maxRequestDurationMsec;
	private double standardDeviationDurationMsec;
	private double meanRequestDurationMsec;
	
	public void setRequestCount(long count) {
		this.requestsCount = count;
	}
	
	public void setMinRequestDurationMsec(long duration) {
		this.minRequestDurationMsec = duration;
	}
	
	public void setMaxRequestDurationMsec(long duration) {
		this.maxRequestDurationMsec = duration;
	}
	
	public void setStandardDeviationDurationMsec(double stdev) {
		this.standardDeviationDurationMsec = stdev;
	}
	
	public void setMeanRequestDurationMsec(double mean) {
		this.meanRequestDurationMsec = mean;
	}
	
	public String toXML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<prtg>");
		sb.append("<result>");
		sb.append("<channel>Request Count</channel>");
		sb.append("<unit>Count</unit>");
		sb.append("<value>" + this.requestsCount + "</value>");
		sb.append("</result>");
		sb.append("<result>");
		sb.append("<channel>Minimum Request Duration</channel>");
		sb.append("<unit>TimeSeconds</unit>");
		sb.append("<speedTime>Seconds</speedTime>");
		sb.append("<value>" + this.minRequestDurationMsec + "</value>");
		sb.append("</result>");
		sb.append("<result>");
		sb.append("<channel>Maximum Request Duration</channel>");
		sb.append("<unit>TimeSeconds</unit>");
		sb.append("<speedTime>speedTime</speedTime>");
		sb.append("<value>" + this.maxRequestDurationMsec + "</value>");
		sb.append("</result>");
		sb.append("<result>");
		sb.append("<channel>Standard Deviation Duration</channel>");
		sb.append("<unit>TimeSeconds</unit>");
		sb.append("<speedTime>speedTime</speedTime>");
		sb.append("<value>" + this.standardDeviationDurationMsec + "</value>");
		sb.append("</result>");
		sb.append("<result>");
		sb.append("<channel>Mean Request Duration</channel>");
		sb.append("<unit>TimeSeconds</unit>");
		sb.append("<speedTime>speedTime</speedTime>");
		sb.append("<value>" + this.meanRequestDurationMsec + "</value>");
		sb.append("</result>");
		sb.append("</prtg>");
		return sb.toString();		
	}
}
