package com.threeshare.rom.replication;

public interface ROMReplication {
    
	public String checkReplicationQueues() throws Exception;
	public String checkReplicationQueues(String exclude, String include) throws Exception;

	public String getReplicationQueueSizes() throws Exception;
	public String getReplicationQueueSizes(String exclude, String include) throws Exception;
}
