package com.threeshare.rom.replication.data;

import java.util.ArrayList;
import java.util.List;
import com.day.cq.replication.Agent;
import com.day.cq.replication.ReplicationQueue;

public class ReplicationAgentData {

    private List<Agent> agents = new ArrayList<Agent>();

    public void addAgent(Agent anAgent) {
        agents.add(anAgent);
    }
    
    @SuppressWarnings("deprecation")
	public String isQueueBlockedXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<prtg>");
        for (Agent anAgent: agents) {
        	ReplicationQueue repQueue = anAgent.getQueue();
        	if (repQueue != null)
        	{
	            sb.append("<result>");
	            sb.append("<channel>" + anAgent.getConfiguration().getName() + "</channel>");
	            sb.append("<Unit>Custom</Unit>");
	            sb.append("<CustomUnit>isBlocked</CustomUnit>");
	            if (repQueue.isBlocked()) {
	            	sb.append("<value>1</value>"); 
	            } else {
	            	sb.append("<value>0</value>");             
	            }
	            sb.append("</result>");
        	}
        }
        
        sb.append("</prtg>");
        return sb.toString();
    }
    
    public String replicationQueueSizes() {
        StringBuilder sb = new StringBuilder();
        sb.append("<prtg>");
        for (Agent anAgent: agents) {
        	ReplicationQueue repQueue = anAgent.getQueue();
        	if (repQueue != null)
        	{
	            sb.append("<result>");
	            sb.append("<channel>" + anAgent.getConfiguration().getName() + "</channel>");
	            sb.append("<Unit>Custom</Unit>");
	            sb.append("<CustomUnit>backlog</CustomUnit>");
	            List<ReplicationQueue.Entry> replicationQueue = repQueue.entries();
	            sb.append("<value>"+ replicationQueue.size() + "</value>");
	            sb.append("</result>");
        	}
        }
        
        sb.append("</prtg>");
        return sb.toString();
    }
}
