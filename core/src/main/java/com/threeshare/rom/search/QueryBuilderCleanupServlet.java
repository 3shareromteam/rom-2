/*
 * Copyright 1997-2008 Day Management AG
 * Barfuesserplatz 6, 4001 Basel, Switzerland
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Day Management AG, ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Day.
 */
package com.threeshare.rom.search;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Dictionary;
import java.util.Iterator;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.jackrabbit.api.security.user.*;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;

@SuppressWarnings("serial")
@Component(metatype = true, immediate = true, label = "Querybuilder Cleanup Servlet", description = "Servlet for node cleanup.")
@Service(value = Servlet.class)
@Properties({
	@Property (name="service.vendor", value="3|SHARE"),
	@Property (name="service.pid", value="com.threeshare.rom.search.QueryBuilderCleanupServlet")
})

public class QueryBuilderCleanupServlet extends SlingAllMethodsServlet {

    private static Logger log = LoggerFactory.getLogger(QueryBuilderCleanupServlet.class);
    
	private static String[] authorizedUsers;
	private static String[] authorizedGroups;
		
	@Property (label = "CleanUp Servlet Path", value = "/bin/querybuilder.cleanup")
	protected static final String SLING_SERVLET_PATHS = "sling.servlet.paths";
	
	@Property (label = "Authorized Users", description = "Users allowed to access cleanup tools", value = { "admin" , ""})
	protected static final String AUTHORIZED_USERS_CONF = "authorizedUsers.value.config";

	@Property (label = "Authorized Groups", description = "Groups allowed to access cleanup tools", value = { "administrators" , ""})
	protected static final String AUTHORIZED_GROUPS_CONF = "authorizedGroups.value.config";
	
    @Reference
    protected QueryBuilder queryBuilder;

	@SuppressWarnings("unchecked")
	@Activate
    protected void activate(ComponentContext context) throws Exception
    {
		// On activation, retrieve values from OSGI configuration.
		final Dictionary<String, Object> properties = context.getProperties();
		authorizedUsers = PropertiesUtil.toStringArray(properties.get(AUTHORIZED_USERS_CONF));
		authorizedGroups = PropertiesUtil.toStringArray(properties.get(AUTHORIZED_GROUPS_CONF));
    }
	
    @Override
    protected void doGet(SlingHttpServletRequest request,
            SlingHttpServletResponse response) throws ServletException,
            IOException {

    	handleRequest(request, response);
    }

    @Override
    protected void doPost(SlingHttpServletRequest request,
            SlingHttpServletResponse response) throws ServletException,
            IOException {
        
        handleRequest(request, response);
    }
    
    private void handleRequest(SlingHttpServletRequest request,
            SlingHttpServletResponse response) throws IOException,
            ServletException {
    	
    	boolean isAuthorized = isAuthorized(request.getResourceResolver());
    	if (isAuthorized)
    	{
    		handleQuery(request, response);
    	}
    	else
    	{
    		// Not Authorized
    		String message = "These aren't the droids you're looking for";
    		int code = 404;
    		writeHtmlResponse (response, message, code);
    	}
    }

    private void handleQuery(SlingHttpServletRequest request,
            SlingHttpServletResponse response) throws IOException,
            ServletException {
        
        response.setContentType("text/html;charset=UTF-8");
        boolean delete = "true".equals(request.getParameter("delete"));
        boolean noDecoration = "true".equals(request.getParameter("noDecoration"));
        Session session = request.getResourceResolver().adaptTo(Session.class);
        Query query = queryBuilder.createQuery(PredicateGroup.create(request.getParameterMap()), session);
        SearchResult result = query.getResult();
        
        try 
        {
        	PrintWriter out = response.getWriter();
        	
        	if (!noDecoration)
        	{
	            StringBuilder sb = new StringBuilder();
	            sb.append("Success: True");
	            sb.append("<br>");
	            sb.append("Results: " + result.getHits().size());
	            sb.append("<br>");
	            sb.append("Total: " + result.getTotalMatches());
	            sb.append("<br>");
	            sb.append("Offset: " + result.getStartIndex());
	            sb.append("<br>");
	            sb.append("Remove: " + delete);
	            sb.append("<br>");
	            sb.append("<br>");

	        	out.println(sb.toString());
        	}
        	        	
            for (Hit hit : result.getHits())
            {
                Resource hResource = hit.getResource();
                if (hResource != null)
                {
                    Node node = hResource.adaptTo(Node.class);
                    boolean nodeFound = (node!=null);
                    String nodePath = node.getPath();
                    
                	StringBuilder sbResponse = new StringBuilder();
                	
                	if (!noDecoration)
                	{
                		sbResponse.append("Node: ");
                	}
                	
                	sbResponse.append(nodePath);
                	sbResponse.append("<br>");
                	out.println(sbResponse.toString());
                    
                    if (nodeFound && delete)
                    {
                    	node.remove();
                        session.save();
                        out.println("Node Deleted");
                        out.println("<br>");
                        out.println("<br>");
                        log.info("Node Deleted via QueryBuilder Cleanup Tool: " + nodePath);
                    }
                    else
                    {
                    	//out.println("Node Deleted");
                    }
                }
            }
        } catch (Exception e) {
        	log.error("Querybuilder.Delete Error:", e);
        } finally {
        	
        }
        
    }

	private boolean isAuthorized(ResourceResolver resourceResolver) 
	{
		boolean isAuthorized = false;
		Authorizable currentUser = resourceResolver.adaptTo(User.class);
		String currentUserID = "";
		try 
		{
			currentUserID = currentUser.getID();
			Iterator<Group> currentUserGroups = currentUser.memberOf();
			
			for (String authorizedUser : authorizedUsers)
			{
				if (authorizedUser.equals(currentUserID))
				{
					isAuthorized = true;
					break;
				}
			}
			
			while(currentUserGroups.hasNext() && !isAuthorized) 
		    {
				Group group = currentUserGroups.next();
				String groupID = group.getID();
				
				for (String authorizedGroup : authorizedGroups)
				{
					if (authorizedGroup.length() > 0 && authorizedGroup.equals(groupID))
					{
						isAuthorized = true;
						break;
					}
				}
					        
		    }
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		
		log.info("QueryBuilder Cleanup Tool called by " + currentUserID);
		return isAuthorized;
	}
   
	private static void writeHtmlResponse(SlingHttpServletResponse response, String msg, int code) throws IOException
	{
		String contentType = "text/html;charset=UTF-8";
		writeResponse(response, msg, code, contentType);
	}
	
	private static void writeResponse(SlingHttpServletResponse response, String msg, int code, String contentType)  throws IOException 
	{
		response.setStatus(code);
		response.setContentType(contentType);
		PrintWriter out = response.getWriter();
		out.println(msg);
		out.close();		
	}
}
