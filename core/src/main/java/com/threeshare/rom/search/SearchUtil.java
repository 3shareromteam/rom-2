package com.threeshare.rom.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.search.Predicate;
import com.day.cq.search.eval.JcrPropertyPredicateEvaluator;
import com.day.cq.search.eval.PathPredicateEvaluator;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;

public class SearchUtil
{
  private static final Logger log = LoggerFactory.getLogger(SearchUtil.class);

  public static List<String> getChildPages (String[] searchPaths, ResourceResolver resourceResolver)
  {
    List<String> pagePathsList = new ArrayList<String>();
    if (searchPaths != null)
    {
      for (String searchPath : searchPaths)
      {
        List<Resource> resourceList = getChildPagesResource (searchPath, resourceResolver);
        
        for (Resource resource : resourceList)
        {
          String path = resource.getPath();
                pagePathsList.add(path);
        }
      }
    }
    return pagePathsList;
    
  }
  
  public static List<String> getChildPages (String searchPath, ResourceResolver resourceResolver)
  {
    List<String> pagePathsList = new ArrayList<String>();
    if (searchPath != null)
    {
      List<Resource> resourceList = getChildPagesResource (searchPath, resourceResolver);
      
      for (Resource resource : resourceList)
      {
        String path = resource.getPath();
              pagePathsList.add(path);
      }
    }
    
    return pagePathsList;
    
  }
  
  public static List<Resource> getChildPagesResource (String searchPath, ResourceResolver resourceResolver)
  {
    String type = "cq:Page";
    return searchByType (searchPath, resourceResolver, type);
    
  }
  
  public static List<String> getDAMAssets (String searchPaths[], ResourceResolver resourceResolver)
  {
    String type = "dam:Asset";
    List<String> damAssetPathsList = new ArrayList<String>();
    if (searchPaths != null)
    {
      for (String searchPath : searchPaths)
      {
        List<Resource> resourceList = searchByType(searchPath, resourceResolver, type);
        for (Resource resource : resourceList)
        {
          damAssetPathsList.add(resource.getPath());
        }
      }
    }
    
    return damAssetPathsList;
  }
  
  public static List<String> getDAMAssets (String searchPath, ResourceResolver resourceResolver)
  {
    String type = "dam:Asset";
    List<String> damAssetPathsList = new ArrayList<String>();
    if (searchPath != null)
    {
      List<Resource> resourceList = searchByType(searchPath, resourceResolver, type);
      
      for (Resource resource : resourceList)
      {
        damAssetPathsList.add(resource.getPath());
      }
    }
    
    return damAssetPathsList;
  }
  
  public static List<Resource> searchByType (String searchPath, ResourceResolver resourceResolver, String type)
  {
    List<Resource> resourceList = new ArrayList<Resource>();
    
    try
    {
      QueryBuilder queryBuilder = resourceResolver.adaptTo(QueryBuilder.class);
      Session session = resourceResolver.adaptTo(Session.class);
  
      Map<String, String> map = new HashMap<String, String>();
      map.put("path", searchPath);
      map.put("p.limit", "100000");
      map.put("type", type);
      
      Query query = queryBuilder.createQuery(PredicateGroup.create(map), session);
      SearchResult result = query.getResult();
  
      for (Hit hit : result.getHits())
        {
            Resource hResource = hit.getResource();
            if (hResource != null)
            {
                resourceList.add(hResource);
            }
        }
    }
    catch(Exception ex)
    {
      log.error("SearchUtil Error for search path:" + searchPath,  ex);
    }
    
    return resourceList;
    
  }
  
  public static List<Node> jcrcontentPropertySearchNode (String key, String value, 
      String searchPath, ResourceResolver resolver)
  {
    
    String subPath = "jcr:content/";
    return propertySearchNode (key, value, searchPath, resolver, subPath);
  }
  
  public static List<Resource> jcrcontentPropertySearchResource (String key, String value, 
      String searchPath, ResourceResolver resolver)
  {
    String subPath = "jcr:content/";
    return propertySearchResource (key, value, searchPath, resolver, subPath);
  }
  
  public static List<Node> propertySearchNode (String key, String value, 
      String searchPath, ResourceResolver resolver)
  {
    String subPath = "";
    return propertySearchNode (key, value, searchPath, resolver, subPath);
  }
  
  public static List<Resource> propertySearchResource (String key, String value, 
      String searchPath, ResourceResolver resolver)
  {
    String subPath = "";
    return propertySearchResource (key, value, searchPath, resolver, subPath);
  }
  
  public static List<Resource> propertySearchResource (String key, String value, 
      String[] searchPaths, ResourceResolver resolver)
  {
    List<Resource> resultsResources = new ArrayList<Resource>();
    
    for(String searchPath: searchPaths)
    {
      List<Resource> resources = propertySearchResource (key, value, searchPath, resolver);
      for (Resource resource : resources)
      {
        if (resource != null)
        {
          resultsResources.add(resource);
        }
      }
    }
    
    return resultsResources;
  }
  
  public static List<Node> propertySearchNode (String key, String value, String searchPath, ResourceResolver resolver, String subPath)
  {
    List<Resource> resources = propertySearchResource (key, value, searchPath, resolver);
    List<Node> nodes = new ArrayList<Node>();
    for (Resource resource : resources)
    {
      Node node = resource.adaptTo(Node.class);
      if (node != null)
      {
        nodes.add(node);
      }
    }
    
    return nodes;
  }
  
  public static List<Resource> propertySearchResource (String key, String value, String searchPath, ResourceResolver resolver, String subPath)
  {
    List<Resource> resources = new ArrayList<Resource>();
    
    // Dont retrive values for null paths
    if (searchPath != null && searchPath.length() > 0)
    {
      try
      {
        QueryBuilder queryBuilder = resolver.adaptTo(QueryBuilder.class);
        Session session = resolver.adaptTo(Session.class);
    
        Map<String, String> map = new HashMap<String, String>();
        map.put("path", searchPath);
        map.put("p.limit", "10000");
        map.put("property",subPath + key);
        
        if (value != null && !value.equals("*"))
        {
          map.put("property",value);
        }
        
        Query query = queryBuilder.createQuery(PredicateGroup.create(map), session);
        SearchResult result = query.getResult();
    
        for (Hit hit : result.getHits())
          {
              Resource hResource = hit.getResource();
              if (hResource != null)
              {
                resources.add(hResource);
              }
          }
        
  
      }
      catch(Exception ex)
      {
        log.error("SearchUtil Error for search path:" + searchPath,  ex);
      }
    }
    
    return resources;
  }

  public static Long assetSearchCount (String searchPath, ResourceResolver resolver, String assetType)
  {
    Long searchCount = new Long(0);
    
    if (searchPath != null && searchPath.length() > 0)
    {
      try
      {
        QueryBuilder queryBuilder = resolver.adaptTo(QueryBuilder.class);
        Session session = resolver.adaptTo(Session.class);
    
        Map<String, String> map = new HashMap<String, String>();
        map.put("path", searchPath);
        map.put("p.limit", "1");
        map.put("type",assetType);
        
        Query query = queryBuilder.createQuery(PredicateGroup.create(map), session);
        SearchResult result = query.getResult();
        
        searchCount = result.getTotalMatches();
        
  
      }
      catch(Exception ex)
      {
        log.error("SearchUtil Error for search path:" + searchPath,  ex);
      }
    }
    
    return searchCount;
  }
  
  /**
     * Return all the resources tagged with at least one of the given tags under the given path
     * @param tags
     * @return
     */
  public static List<Node> getTaggedResources(List<String> tags, String path, ResourceResolver resourceResolver)
  {
    String[] tagsArray = tags.toArray(new String[tags.size()]);
    return getTaggedResources(tagsArray, path, resourceResolver);
  }
  
    /**
     * Return all the resources tagged with at least one of the given tags under the given path
     * @param tags
     * @return
     */
    public static List<Node> getTaggedResources(String[] tags, String path, ResourceResolver resourceResolver){
        if (tags==null || tags.length == 0)
            return null;

        Predicate tagsPredicate = new Predicate("tag", JcrPropertyPredicateEvaluator.PROPERTY);
        tagsPredicate.set(JcrPropertyPredicateEvaluator.PROPERTY, "cq:tags");
        for (int i = 0; i < tags.length; i++)
            tagsPredicate.set(i + 1 + "_value", "%" + tags[i] + "%");
        tagsPredicate.set(JcrPropertyPredicateEvaluator.OPERATION,
                JcrPropertyPredicateEvaluator.OP_LIKE);

        Predicate pathPredicate = new Predicate(PathPredicateEvaluator.PATH, PathPredicateEvaluator.PATH);
        pathPredicate.set(PathPredicateEvaluator.PATH, path);

        PredicateGroup predicates = new PredicateGroup();
        predicates.add(tagsPredicate);
        predicates.add(pathPredicate);
        // create the search query.
        Query query = resourceResolver.adaptTo(QueryBuilder.class).createQuery(predicates, resourceResolver.adaptTo(Session.class));
        query.setHitsPerPage(0);

        // execute the query and return the results
        SearchResult searchResult = query.getResult();
        List<Node> result = new ArrayList<Node>();
        // Now we just convert the hits into nodes and return them.
        for (Hit hit : searchResult.getHits()) {
            try {
                Node node = hit.getResource().adaptTo(Node.class);
                while (node!=null && !"dam:Asset".equals(node.getProperty(JcrConstants.JCR_PRIMARYTYPE).getString())
                                  && !"cq:Page".equals(node.getProperty(JcrConstants.JCR_PRIMARYTYPE).getString())){
                    node = node.getParent();
                }
                if (node!=null){
                    result.add(node);
                }

            } catch (Exception e) {
            }
        }
        return result;
    }
}
