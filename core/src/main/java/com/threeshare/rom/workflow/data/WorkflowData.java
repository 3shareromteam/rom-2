/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.threeshare.rom.workflow.data;

import java.util.Calendar;

/**
 *
 * @author rbrown
 */
public class WorkflowData {
   private String nodeName;
   private String modelTitle;
   private Calendar startTime;
   private String initiator;
   private String userId;
   private String resourcePath;

    public String getInitiator() {
        return initiator;
    }

    public String getModelTitle() {
        return modelTitle;
    }

    public String getNodeName() {
        return nodeName;
    }

    public String getResourcePath() {
        return resourcePath;
    }

    public Calendar getStartTime() {
        return startTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    public void setModelTitle(String modelTitle) {
        this.modelTitle = modelTitle;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public void setResourcePath(String resourcePath) {
        this.resourcePath = resourcePath;
    }

    public void setStartTime(Calendar startTime) {
        this.startTime = startTime;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
   
    public String toResultXML() {
        StringBuilder xml = new StringBuilder();
        xml.append("<result>");
        xml.append("<channel>Node Name</channel>");
	xml.append("<value>" + this.nodeName + "</value>");
        xml.append("</result>");
        xml.append("<result>");
        xml.append("<channel>Model Title</channel>");
	xml.append("<value>" + this.modelTitle + "</value>");
        xml.append("</result>");
        xml.append("<result>");
        xml.append("<channel>Start Time</channel>");
	xml.append("<value>" + this.startTime + "</value>");
        xml.append("</result>");
        xml.append("<result>");
        xml.append("<channel>UserId</channel>");
	xml.append("<value>" + this.userId + "</value>");
        xml.append("</result>");
        xml.append("<result>");
        xml.append("<channel>Initiator</channel>");
	xml.append("<value>" + this.initiator + "</value>");
        xml.append("</result>");
        xml.append("<result>");
        xml.append("<channel>Resource Path</channel>");
	xml.append("<value>" + this.resourcePath + "</value>");
        xml.append("</result>");
        return xml.toString();
    }
    
    public String toXML() {
        StringBuilder xml = new StringBuilder();
        xml.append("<prtg>");
        xml.append(toResultXML());
        xml.append("</prtg>");
        return xml.toString();
    }
}
