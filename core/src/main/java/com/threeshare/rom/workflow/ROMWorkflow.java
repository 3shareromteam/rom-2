package com.threeshare.rom.workflow;

public interface ROMWorkflow {
    public String getWorkflowsInError() throws Exception;
    
    public String getLongStandingWorkflows(String model, int timeout) throws Exception;
    
    public int getRunningWorkflowCount() throws Exception;
}
