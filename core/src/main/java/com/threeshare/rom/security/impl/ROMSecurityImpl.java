package com.threeshare.rom.security.impl;


import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.osgi.framework.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.threeshare.rom.security.ROMSecurity;
import com.threeshare.rom.bundles.ROMBundleContext;
import com.threeshare.rom.util.ROMConstants;

@Component
@Service
@Properties({
    @Property(name="service.description", value="ROM Security Data Collector"),
    @Property(name="service.vendor", value="3|SHARE")
})
public class ROMSecurityImpl implements ROMSecurity {
    
    private final Logger log = LoggerFactory.getLogger(getClass());
    
    public boolean isTxtRendererEnabled() throws IllegalArgumentException {
		Bundle defaultGetServletBundle = ROMBundleContext.getBundleContext().getBundle(ROMConstants.DEFAULT_GET_SERVLET_BUNDLE_ID);
		if (defaultGetServletBundle != null) {
			String isEnabled = defaultGetServletBundle.getBundleContext().getProperty("enable.txt");
	    	log.info("enable.txt = " + isEnabled);
		    if (isEnabled != null && !isEnabled.isEmpty()) {
		    	return Boolean.getBoolean(isEnabled);
		    } else {
		    	return false;
		    }
		} else {
			throw new IllegalArgumentException("Can not find bundle with id = " + ROMConstants.DEFAULT_GET_SERVLET_BUNDLE_ID);
		}
    }
}
