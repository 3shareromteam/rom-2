package com.threeshare.rom.security;

public interface ROMSecurity {
	public boolean isTxtRendererEnabled() throws IllegalArgumentException;
}
