package com.threeshare.rom.config;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.threeshare.rom.util.ROMConstants;

public class ROMConfigManager
{
	private static final Logger log = LoggerFactory.getLogger(ROMConfigManager.class);
	private static final String CONFIGURATIONS_NODE_NAME = "configurations";
	private static final HashMap<String, String> CONFIGS = new HashMap<String, String>();
	private static final HashMap<String, String[]> CONFIGS_MULTI = new HashMap<String, String[]>();
	private ResourceResolver resolver;
	private Session session;
	private Node configNode;
		
	// In order to add a config to the ROM Config Manager, first define a property
	// and then add a default value
	public static final String SERVLET_PATH = "servletPath";
	public static final String EXTERNAL_VANITY = "externalVanity";
    
	static
	{
		// Set Default Single Value Example 	
		CONFIGS.put(SERVLET_PATH, "/bin/vanity");
		
		// Set Default Multi Value Example
		CONFIGS_MULTI.put(EXTERNAL_VANITY, new String[]{});
	}

	public ROMConfigManager(ResourceResolver resolver)
	{
		this.resolver = resolver;
		session = resolver.adaptTo(Session.class);
	}

	public String getConfig(String configName)
	{
		String value = null;

		getConfigNode(false);

		try
		{
			if (configNode != null && configNode.hasProperty(configName))
			{
				value = configNode.getProperty(configName).getString();
			}
			else
			{
				value = getDefaultValue(configName);
			}
		}
		catch (Exception ex)
		{
			log.error("Cannot read system configuration", ex);
		}

		return value;
	}

	public String getConfig(String configName, String defaultValue)
	{
		String value = null;

		getConfigNode(false);

		try
		{
			if (configNode != null && configNode.hasProperty(configName))
			{
				value = configNode.getProperty(configName).getString();
			}
			else
			{
				value = defaultValue;
			}
		}
		catch (Exception ex)
		{
			log.error("Cannot read system configuration", ex);
		}

		return value;
	}
	
	public String getConfig(String configName, String defaultValue, boolean allowEmptyValue)
	{
		String value = getConfig(configName, defaultValue);
		
		if (!allowEmptyValue)
		{
			if (value == null || value.length() == 0)
			{
				value = defaultValue;
			}
		}
				
		return value;
	}
	
	
	public String[] getMultiConfig(String configName)
	{
		String[] values = null;

		getConfigNode(false);

		try
		{
			if (configNode != null && configNode.hasProperty(configName))
			{
				Property property = configNode.getProperty(configName);
				if (property.isMultiple())
				{
					Value[] theValues = property.getValues();
					values = new String[theValues.length];
					for (int i = 0; i < theValues.length; i++)
					{
						values[i] = theValues[i].getString();
					}
				}
			}
			else
			{
				values = getDefaultValues(configName);
			}
		}
		catch (Exception ex)
		{
			log.error("Cannot read system configuration", ex);
		}

		return values;
	}

	public void setConfig(String configName, String configValue) throws Exception
	{
		if (CONFIGS.get(configName) == null)
		{
			throw new Exception("Unknown Config Property ["+configName+"]");
		}

		getConfigNode(true);

		try
		{
			configNode.setProperty(configName, (configValue != null ? configValue.trim() : null));
			session.save();
		}
		catch (Exception ex)
		{
			log.error("Cannot set system configuration", ex);
			
			throw new Exception("CANNOT.SET.SYSTEM.CONFIGURATION");
		}
	}

	public void setMultiConfig(String configName, String[] configValues) throws Exception
	{
		if (CONFIGS_MULTI.get(configName) == null)
		{
			throw new Exception("Unknown Config Property ["+configName+"]");
		}

		getConfigNode(true);

		try
		{
			configNode.setProperty(configName, (configValues != null ? configValues : null));
			session.save();
		}
		catch (Exception ex)
		{
			log.error("Cannot set system configuration", ex);
			
			throw new Exception("CANNOT.SET.SYSTEM.CONFIGURATION");
		}
	}
	
	public void deleteConfig(String configName) throws Exception
	{
		getConfigNode(true);
		try
		{
			if (configNode.hasProperty(configName))
			{
				Property prop = configNode.getProperty(configName);
				prop.remove();
				session.save();
			}
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);
			
			throw new Exception(ex);
		}
	}

	public void setConfig(HashMap<String, String> configMap) throws Exception
	{
		try
		{
			Set<String> keySet = configMap.keySet();
			Iterator<String> iter = keySet.iterator();
			while (iter.hasNext())
			{
				String key = iter.next();
				String value = configMap.get(key);
				setConfig(key, value);
				log.info("setting " + key + " to " + value);
			}
		}
		catch (Exception ex)
		{
			log.error("Cannot set system configuration", ex);
			
			throw new Exception("CANNOT.SET.SYSTEM.CONFIGURATION");
		}
	}
	
	private Node getConfigNode(boolean createIfNotExists)
	{
		if (configNode == null)
		{
			try
			{
				Node etcNode = resolver.resolve(
					ROMConstants.ETC_NODE_PATH).adaptTo(Node.class);
				Node rootConfigNode = findNode(etcNode,
					ROMConstants.CONFIG_NODE_NAME, createIfNotExists);
				Node etvConfigNode = findNode(rootConfigNode,
					ROMConstants.ROM_CONFIG_NODE_NAME, createIfNotExists);
				configNode = findNode(etvConfigNode,
					CONFIGURATIONS_NODE_NAME, createIfNotExists);
			}
			catch (Exception ex)
			{
				log.error(ex.toString(), ex);
			}
		}
		
		return configNode;
	}

	private Node findNode(Node parentNode, String nodeName, boolean createIfNotExists)
		throws RepositoryException
	{
		Node node = null;
		if (parentNode != null)
		{
			if (parentNode.hasNode(nodeName))
			{
				node = parentNode.getNode(nodeName);
			}
			else if (createIfNotExists)
			{
				node = parentNode.addNode(nodeName, JcrConstants.NT_UNSTRUCTURED);
			}
		}
		return node;
	}

	private String getDefaultValue(String config)
	{
		return CONFIGS.get(config);
	}
	
	private String[] getDefaultValues(String config)
	{
		return CONFIGS_MULTI.get(config);
	}
}
