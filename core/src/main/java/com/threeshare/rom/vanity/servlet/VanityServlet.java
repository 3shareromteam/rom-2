/*************************************************************************

  Copyright 2013 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

**************************************************************************/
package com.threeshare.rom.vanity.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Dictionary;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.threeshare.rom.util.ROMConstants;
import com.threeshare.rom.util.RequestUtils;
import com.threeshare.rom.vanity.VanityFactory;
import com.threeshare.rom.vanity.VanityUtils;

@Component(metatype = true, immediate = true, label = "3|SHARE ROM Vanity Servlet", description = "Service to respond to vanity system requests")
@Service(value = Servlet.class)
@Properties({
	@Property (name="service.vendor", value="ROM Vanity Servlet"),
	@Property (name="service.pid", value="com.threeshare.rom.vanity.servlet.VanityServlet")
})

public class VanityServlet extends SlingAllMethodsServlet
{
	private static final long serialVersionUID = -2545424501424665012L;
	private static final Logger log = LoggerFactory.getLogger(VanityServlet.class);
	private static String successMessage;
	public static String vanityConfigurationPath = "";
	public static boolean duplicateCheckEnabled = false;
	
	
	@Property (label = "Servlet Paths", description = "Active servlet paths. All paths must be in the sling execution paths in order to respond.", value = { "/bin/threeshare/vanity_check" , "/bin/threeshare/vanity" })
	protected static final String SERVLET_PATHS = "sling.servlet.paths";
	
	@Property (label = "Vanity Configuration Path", description = "Path for system wide vanity configuration.", value = "/etc/config/vanity")
	protected static final String VANITY_CONFIG_PATH = "vanity.config.path";
	
	@Property (label = "Success Message", description = "Message returned by the servlet for successful transaction.", value = "SUCCESS")
	protected static final String SERVLET_SUCCESS_MESSAGE = "vanity.config.success.message";
	
	@Property (label = "Duplicate Vanity Check Enabled", description = "Option to enable/disable the duplicate vanity check. ", boolValue = false)
	protected static final String DUPLICATE_VANITY_CHECK_ENABLED = "vanity.config.enabled";
	
	@Activate
    protected void activate(ComponentContext context) throws Exception
    {
		// On activation, retrieve values from OSGI configuration.
		@SuppressWarnings("unchecked")
		final Dictionary<String, Object> properties = context.getProperties();
		successMessage = PropertiesUtil.toString(properties.get(SERVLET_SUCCESS_MESSAGE), "");
		vanityConfigurationPath = PropertiesUtil.toString(properties.get(VANITY_CONFIG_PATH), "");
		String duplicateCheckEnabledValue = PropertiesUtil.toString(properties.get(DUPLICATE_VANITY_CHECK_ENABLED), "true").toLowerCase();
		duplicateCheckEnabled = duplicateCheckEnabledValue.equals("true");
    }
	
	protected void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException
	{
		try
		{
			if (isValidRequest(request) == true)
			{
				String requestType = RequestUtils.getParameter(request, ROMConstants.REQUEST_TYPE_PARAM);
				String json = RequestUtils.getParameter(request, ROMConstants.POST_JSON_VANITY_PARAM);
				String configNodePath = RequestUtils.getParameter(request, ROMConstants.POST_JSON_CONFIG_PARAM);
				
				ResourceResolver resolver = request.getResourceResolver();
				
				if (requestType.equals(ROMConstants.UPDATE_VANITY_REQUEST_TYPE))
				{
					// Update vanity request handling
					String responseMessage = VanityUtils.processVanityUpdate(resolver, json, configNodePath);
					
					if (responseMessage == null || responseMessage.length() == 0)
					{
						writeHtmlResponse(response, successMessage, 200);
						return;
					}
					else
					{
						writeHtmlResponse(response, responseMessage, 500);
						return;
					}
				}
				else if (requestType.equals(ROMConstants.DELETE_VANITY_REQUEST_TYPE))
				{
					// Remove vanity request handling
					boolean delete = VanityUtils.processVanityDelete(resolver, json, configNodePath);
					if (!delete)
					{
						writeValidResponse(response, "Vanity delete failed");
						response.setStatus(500);
						return;
					}
				}
			}
			else
			{
				writeInvalidResponse(response, "Invalid Request");
				return;
			}
	
			String message = "Complete";
			writeValidResponse(response, message);
		
		}
		catch (Exception e)
		{
			String message = "Error occured during vanity request processing.";
			log.error (message,e);
			writeHtmlResponse(response, message, 500);
		}
	}
	
	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException
	{
		
		try
		{
			if (isValidRequest(request) == true)
			{
				String requestType = RequestUtils.getParameter(request, ROMConstants.REQUEST_TYPE_PARAM);
				
				if (requestType.equals(ROMConstants.VANITY_CHECK_REQ_TYPE_PARAM))
				{
					// Vanity check request handling
					String vanityURI = RequestUtils.getParameter(request, ROMConstants.VANITY_CHECK_VANITY_PARAM);
					String path = RequestUtils.getParameter(request, ROMConstants.VANITY_CHECK_PATH_PARAM);
					
					ResourceResolver resolver = request.getResourceResolver();
					
					boolean vanityExists = VanityUtils.processVanityCheck(resolver, vanityURI, path);
					if(vanityExists)
					{
						// Set Response for valid vanity
						String message = "Duplicate vanity detected. Vanity: " + vanityURI;
						writeHtmlResponse(response, message, 412);
						return;
					}
					else
					{
						// Set Response for invalid vanity
						String message = "Valid Vanity";
						writeHtmlResponse(response, message,200);
						return;
					}
				}
			}
			else
			{
				writeInvalidResponse(response, "Invalid Request");
				return;
			}
				
			String message = "Complete";
			writeValidResponse(response, message);
		
		}
		catch (Exception e)
		{
			String message = "Error occured during vanity request processing.";
			log.error (message,e);
			writeHtmlResponse(response, message, 500);
		}
	}

	private boolean isValidRequest(SlingHttpServletRequest request)
	{
		// Check for Authenticated User
		String username = RequestUtils.getRequestUser(request);
		
		if (username == null ||	ROMConstants.ANONYMOUS_USER.equalsIgnoreCase(username))
		{
			log.error("Vanity Servlet: Must be authenticated.");
			return false;
		}
		
		String method = request.getMethod();
		if (method.equals("POST"))
		{
			return isValidPost(request);
		}
		else if (method.equals("GET"))
		{
			return isValidGet(request);
		}
		
		return true;
	}
	
	private boolean isValidPost(SlingHttpServletRequest request)
	{
		// Check to ensure all parameters are present before processing
		String requestType = RequestUtils.getParameter(request, ROMConstants.REQUEST_TYPE_PARAM);
		String json = RequestUtils.getParameter(request, ROMConstants.POST_JSON_VANITY_PARAM);
		String configNodePath = RequestUtils.getParameter(request, ROMConstants.POST_JSON_CONFIG_PARAM);
		
		try 
		{
			if (requestType == null)
			{
				log.error("Vanity POST Servlet: No request type detected.");
				return false;
			}
			else if (requestType.equals(ROMConstants.UPDATE_VANITY_REQUEST_TYPE))
			{
				// Check necessary parameters for Vanity Update/Add
				if (json == null)
				{
					log.error("Vanity Update/Add: Missing json request");
					return false;
				}
				
				if (configNodePath == null)
				{
					log.error("Vanity Update/Add: Missing config path.");
					return false;
				}
				else
				{
					ResourceResolver resolver = request.getResourceResolver();
					if (VanityFactory.getConfigNode(resolver, configNodePath) == null)
					{
						log.error("Vanity Update/Add: Invalid Config Path");
						return false;
					}
				}
			}
			else if (requestType.equals(ROMConstants.DELETE_VANITY_REQUEST_TYPE))
			{
				// Check necessary parameters for Vanity Delete
				if (json == null)
				{
					log.error("Vanity Delete: Missing json request");
					return false;
				}
				
				if (configNodePath == null)
				{
					log.error("Vanity Delete: Missing config path.");
					return false;
				}
				else
				{
					ResourceResolver resolver = request.getResourceResolver();
					if (VanityFactory.getConfigNode(resolver, configNodePath) == null)
					{
						log.error("Vanity Delete: Invalid Config Path");
						return false;
					}
				}
			}
			else 
			{
				log.error("Vanity POST Servlet: Unknown request type.");
				return false;
			}
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);
		}
		
		
		return true;
	}
	
	private boolean isValidGet(SlingHttpServletRequest request)
	{
		// Check to ensure all parameters are present before processing
		String requestType = RequestUtils.getParameter(request, ROMConstants.REQUEST_TYPE_PARAM);
		boolean isValid = true;
		
		try 
		{
			if (requestType == null || requestType.length() == 0)
			{
				log.error("Vanity Check: No request type detected.");
				return false;
			}
			else if (requestType.equals(ROMConstants.VANITY_CHECK_REQ_TYPE_PARAM))
			{
				// For vanity checks validate necessary parameters
				String vanityURI = RequestUtils.getParameter(request, ROMConstants.VANITY_CHECK_VANITY_PARAM);
				String path = RequestUtils.getParameter(request, ROMConstants.VANITY_CHECK_PATH_PARAM);
				
				if (vanityURI == null || vanityURI.length() == 0)
				{
					log.error("Vanity Check: Missing vanity parameter.");
					return false;
				}
				else if (path == null || path.length() == 0)
				{
					log.error("Vanity Check: Missing path parameter.");
					return false;
				}
			}
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);
			return false;
		}

		return isValid;
	}
		
	private static void writeValidResponse(SlingHttpServletResponse response, String responseMessage)  throws IOException 
	{
		int responseCode = 200;
		writeHtmlResponse(response, responseMessage, responseCode);		
	}
	
	private static void writeInvalidResponse(SlingHttpServletResponse response, String responseMessage)  throws IOException 
	{
		int responseCode = 412;
		writeHtmlResponse(response, responseMessage, responseCode);			
	}
	
	private static void writeHtmlResponse(SlingHttpServletResponse response, String msg, int code) throws IOException
	{
		String contentType = "text/html;charset=UTF-8";
		writeResponse(response, msg, code, contentType);
	}
	
	@SuppressWarnings("unused")
	private static void writeJsonResponse(SlingHttpServletResponse response, String msg, int code) throws IOException
	{
		String contentType = "application/json;charset=UTF-8";
		writeResponse(response, msg, code, contentType);
	}
	
	@SuppressWarnings("unused")
	private static void writeXmlResponse(SlingHttpServletResponse response, String msg, int code) throws IOException
	{
		String contentType = "application/xml;charset=UTF-8";
		writeResponse(response, msg, code, contentType);
	}
	
	private static void writeResponse(SlingHttpServletResponse response, String msg, int code, String contentType)  throws IOException 
	{
		response.setStatus(code);
		response.setContentType(contentType);
		PrintWriter out = response.getWriter();
		out.println(msg);
		out.close();		
	}
}