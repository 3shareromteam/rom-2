/*************************************************************************

  Copyright 2013 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

**************************************************************************/

package com.threeshare.rom.vanity;

import java.util.Comparator;

import javax.jcr.Node;

import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.threeshare.rom.util.AbstractNodeWrapper;
import com.threeshare.rom.util.ROMConstants;
import com.threeshare.rom.util.ROMStringUtils;

public class VanityNode extends AbstractNodeWrapper
{
	private static final Logger log = LoggerFactory.getLogger(VanityNode.class);
	
	public VanityNode(ResourceResolver resolver, Node node) {
		super(resolver, node);
	}
	
	public String getVanityTarget()
	{
		String vanityTarget = "";
		try
		{
			Node contentNode = getNode().getNode(JcrConstants.JCR_CONTENT);
			if (contentNode.hasProperty(ROMConstants.TARGET_URL_PROPERTY))
			{
				vanityTarget = contentNode.getProperty(ROMConstants.TARGET_URL_PROPERTY).getString();
			}
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);

		}
		return vanityTarget;
	}
	
	public void setVanityTarget(String target)
    {
        try
        {
            Node vanityContentNode = getContentNode();
            vanityContentNode.setProperty(
            		ROMConstants.TARGET_URL_PROPERTY, target);
            vanityContentNode.getSession().save();
            
        }
        catch (Exception ex)
        {
            log.error(ex.toString(), ex);
        }
    }
	
	public String getVanityURI()
	{
		String vanityURI = "";
		try
		{
			Node contentNode = getNode().getNode(JcrConstants.JCR_CONTENT);
			if (contentNode.hasProperty(ROMConstants.VANITY_URL_PROPERTY))
			{
				vanityURI = contentNode.getProperty(ROMConstants.VANITY_URL_PROPERTY).getString();
			}
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);

		}
		return vanityURI;
	}
	
	public void setVanityURI(String vanity)
    {
        try
        {
            Node vanityContentNode = getContentNode();
            vanityContentNode.setProperty(
            		ROMConstants.VANITY_URL_PROPERTY, vanity);
            vanityContentNode.getSession().save();
            
        }
        catch (Exception ex)
        {
            log.error(ex.toString(), ex);
        }
    }
	
	public String getRedirectType()
	{
		String vanityURI = "";
		try
		{
			Node contentNode = getNode().getNode(JcrConstants.JCR_CONTENT);
			if (contentNode.hasProperty(ROMConstants.REDIRECT_TYPE_PROPERTY))
			{
				vanityURI = contentNode.getProperty(ROMConstants.REDIRECT_TYPE_PROPERTY).getString();
			}
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);

		}
		return vanityURI;
	}
	
	public void setRedirectType(String vanity)
    {
        try
        {
            Node vanityContentNode = getContentNode();
            vanityContentNode.setProperty(
            		ROMConstants.REDIRECT_TYPE_PROPERTY, vanity);
            vanityContentNode.getSession().save();
            
        }
        catch (Exception ex)
        {
            log.error(ex.toString(), ex);
        }
    }

	public VanityConfigNode getConfig()
	{
		try
		{	
			// Find parent node that is the config node.
			boolean configFound = false;
			int maxLevelsToSearch = 4;
			int count = 0;
			Node node = getNode();
			while (!configFound && count < maxLevelsToSearch)
			{
				node = node.getParent();
				configFound = VanityUtils.isConfigNode(node);
				count++;
			}
			
			if (configFound)
			{
				return new VanityConfigNode(resolver, node);
			}
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);
		}
		return null;
	}
	
	public static Comparator<VanityNode> vanityURIComparatorAsc = new Comparator<VanityNode>()  
    {
		public int compare(VanityNode vanity1, VanityNode vanity2) 
		{
		String vanityName1 = ROMStringUtils.cleanPath(vanity1.getVanityURI()).toUpperCase();
		String vanityName2 = ROMStringUtils.cleanPath(vanity2.getVanityURI()).toUpperCase();
		
		//ascending order
		return vanityName1.compareTo(vanityName2);
		
		}
    };
	
	public static Comparator<VanityNode> vanityURIComparatorDesc = new Comparator<VanityNode>()  
    {
		public int compare(VanityNode vanity1, VanityNode vanity2) 
		{
			
		String vanityName1 = ROMStringUtils.cleanPath(vanity1.getVanityURI()).toUpperCase();
		String vanityName2 = ROMStringUtils.cleanPath(vanity2.getVanityURI()).toUpperCase();
		
		//descending order
		return vanityName2.compareTo(vanityName1);
		
		}
    };    
}