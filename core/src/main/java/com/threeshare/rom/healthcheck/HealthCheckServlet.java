/*************************************************************************

  Copyright 2013 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

**************************************************************************/
package com.threeshare.rom.healthcheck;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Dictionary;

import javax.servlet.ServletException;
import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.osgi.PropertiesUtil;

import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(metatype = true, immediate = true, label = "3|SHARE ROM Health Check Servlet", description = "Service to respond to AEM health check requests")
@Service(value = Servlet.class)
@Properties({
	@Property (name="service.vendor", value="ROM Health Check Servlet"),
	@Property (name="service.pid", value="com.threeshare.rom.healthcheck")
})
	
public class HealthCheckServlet extends SlingAllMethodsServlet
{
	private static final long serialVersionUID = -8113276270892439174L;
	private static final Logger log = LoggerFactory.getLogger(HealthCheckServlet.class);
	private String successMessage;
	private String failedMessage;
	private boolean healthCheckEnabled = true;
	
	@Property (label = "Health Check Enabled", description = "Option to enable/disable the health check. ", boolValue = false)
	protected static final String SERVLET_HEALTHCHECK_ENABLED = "servlet.healthcheck.enabled";
	
	@Property (label = "Servlet Paths", description = "Active servlet paths. All paths must be in the sling execution paths in order to respond.", value = { "/bin/threeshare/system_health" , "/bin/threeshare/health_check" })
	protected static final String SERVLET_SELECTORS = "sling.servlet.paths";
	
	@Property (label = "Success Message", description = "Message returned by the servlet for successful health check.", value = "SUCCESS")
	protected static final String SERVLET_SUCCESS_MESSAGE = "servlet.success.message";
	
	@Property (label = "Failure Message", description = "Message returned by the servlet for failed health check.", value = "FAILED")
	protected static final String SERVLET_FAILURE_MESSAGE = "servlet.failure.message";
		
	@Activate
    protected void activate(ComponentContext context) throws Exception
    {
		// On activation, retrieve values from OSGI configuration.
		@SuppressWarnings("unchecked")
		final Dictionary<String, Object> properties = context.getProperties();
		successMessage = PropertiesUtil.toString(properties.get(SERVLET_SUCCESS_MESSAGE), "");
		failedMessage = PropertiesUtil.toString(properties.get(SERVLET_FAILURE_MESSAGE), "");
		String healthCheckValue = PropertiesUtil.toString(properties.get(SERVLET_HEALTHCHECK_ENABLED), "true").toLowerCase();
		healthCheckEnabled = healthCheckValue.equals("true");
	}
	
	protected void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException
	{
		
		String responseMessage = successMessage;
		int responseCode = 200;
		
		if (!healthCheckEnabled)
		{
			responseMessage = failedMessage;
			responseCode = 404;
		}
		
		log.debug("Call made to health check servlet. Servlet responded with \"" + responseMessage +  "\"");
		writeHtmlResponse(response, responseMessage, responseCode);
	}
	
	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException
	{
		
		String responseMessage = successMessage;
		int responseCode = 200;
		
		if (!healthCheckEnabled)
		{
			responseMessage = failedMessage;
			responseCode = 404;
		}
		
		log.debug("Call made to health check servlet. Servlet responded with \"" + responseMessage +  "\"");
		writeHtmlResponse(response, responseMessage, responseCode);
	}

	private static void writeHtmlResponse(SlingHttpServletResponse response, String msg, int code)  throws IOException 
	{
		response.setStatus(code);
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.println(msg);
		out.close();		
	}
}