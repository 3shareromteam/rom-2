/*************************************************************************

  Copyright 2013 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

**************************************************************************/
package com.threeshare.rom.healthcheck;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Dictionary;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.Servlet;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(metatype = true, immediate = true, label = "3|SHARE ROM Header Check Servlet", description = "Service to respond to AEM header check requests")
@Service(value = Servlet.class)
@Properties({
	@Property (name="service.vendor", value="ROM Header Check Servlet"),
	@Property (name="service.pid", value="com.threeshare.rom.healthcheck")
})
	
public class HeaderCheckServlet extends SlingAllMethodsServlet
{
	private static final long serialVersionUID = -8113276270892439173L;
	private static final Logger log = LoggerFactory.getLogger(HeaderCheckServlet.class);
	private boolean headerCheckEnabled = true;
	
	@Property (label = "Header Check Enabled", description = "Option to enable/disable the header check. ", boolValue = false)
	protected static final String SERVLET_HEADERCHECK_ENABLED = "servlet.headercheck.enabled";
	
	@Property (label = "Servlet Paths", description = "Active servlet paths. All paths must be in the sling execution paths in order to respond.", value = { "/bin/threeshare/headercheck" })
	protected static final String SERVLET_SELECTORS = "sling.servlet.paths";
		
	@Activate
    protected void activate(ComponentContext context) throws Exception
    {
		// On activation, retrieve values from OSGI configuration.
		@SuppressWarnings("unchecked")
		final Dictionary<String, Object> properties = context.getProperties();
		String headerCheckValue = PropertiesUtil.toString(properties.get(SERVLET_HEADERCHECK_ENABLED), "true").toLowerCase();
		headerCheckEnabled = headerCheckValue.equals("true");
	}
	
	protected void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException
	{
		
		
		String responseMessage = getHeaders(request);
		int responseCode = 200;
		
		if (!headerCheckEnabled)
		{
			responseMessage = "";
			responseCode = 404;
		}
		
				
		log.debug("Call made to header check servlet. Servlet responded with \"" + responseMessage +  "\"");
		writeHtmlResponse(response, responseMessage, responseCode);
	}
	
	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException
	{
		
		String responseMessage = getHeaders(request);
		int responseCode = 200;
		
		if (!headerCheckEnabled)
		{
			responseMessage = "";
			responseCode = 404;
		}
				
		log.debug("Call made to header check servlet. Servlet responded with \"" + responseMessage +  "\"");
		writeHtmlResponse(response, responseMessage, responseCode);
	}

	private String getHeaders(SlingHttpServletRequest request) {
		
		StringBuilder sb = new StringBuilder();
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
        	sb.append("<br>");
            String headerName = headerNames.nextElement();
            sb.append("Header: " + headerName);
            sb.append("<br>");

            Enumeration<String> headers = request.getHeaders(headerName);
            while (headers.hasMoreElements()) {
                String headerValue = headers.nextElement();
                sb.append("Value: " + headerValue);
                sb.append("<br>");

            }
         }
		return sb.toString();
	}

	private static void writeHtmlResponse(SlingHttpServletResponse response, String msg, int code)  throws IOException 
	{
		response.setStatus(code);
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.println(msg);
		out.close();		
	}
}