# 3|SHARE ROM Monitoring Package

This project contains the code used for the 3|SHARE ROM support package. This package contains a mix of PRTG Monitoring sensors, common tools and SEO utilities. 

## PRTG Sensors

#### Revision Cleanup Sensor
**Usage:**  Used to determine the status of the last Revision Cleanup (Online Compaction). Status can be seen on AEM in the daily maintenance page at http://localhost:4502/libs/granite/operations/content/maintenanceWindow.html/apps/settings/granite/operations/maintenance/granite_daily.  
**Monitoring URL:**  /etc/threeshare/revisionCleanupMonitor.xml  
**Compatibility:**  AEM 6.3+  

#### Replication Queues Blocked Sensor
**Usage:**  Used to determine if any replication queues on are blocked. Status can be seen on AEM in the replication page  dail maintenance page at http://localhost:4502/etc/replication.html.  
**Monitoring URL:**  /etc/threeshare/replication.xml  
**Compatibility:**  AEM 6.0+  

#### Replication Queue Size Sensor
**Usage:**  Used to determine the size of the replication queues. Status can be seen on AEM in the replication page  dail maintenance page at http://localhost:4502/etc/replication.html.  
**Monitoring URL:**  /etc/threeshare/replicationQueue.xml  
**Compatibility:**  AEM 6.0+  

#### Online Backup Status Sensor
**Usage:**  Used to determine the status of the last online backup. Status can be seen on AEM in the JMX Repository page at http://localhost:4502/system/console/jmx/org.apache.jackrabbit.oak%3Aname%3Drepository+manager%2Ctype%3DRepositoryManagement.  
**Monitoring URL:**  /etc/threeshare/backup.xml  
**Compatibility:**  AEM 6.0+  

#### JVM Heap Size Sensor
**Usage:**  Used to expose JVM heap memory usage to the monitoring system. Status can be seen on AEM in the JMX memory page at http://localhost:4502/system/console/jmx/java.lang%3Atype%3DMemory  
**Monitoring URL:**  /etc/threeshare/system/jcr:content/jvmdata.xml  
**Compatibility:**  AEM 6.0+  

#### JVM CPU Thread Monitor Sensor
**Usage:**  Used to expose the number of CPU Threads used by the JVM to the monitoring system.   
**Monitoring URL:**  /etc/threeshare/system/jcr:content/threaddata.xml  
**Compatibility:**  AEM 6.0+  

#### Sling Metrics Sensor - *DEPRECATED*
**Usage:**  Used to expose data from the Sling Request Processer Engine to the monitoring system. Status can be seen on AEM in the JMX Sling Engine page at http://localhost:4502/system/console/jmx/org.apache.sling%3Aservice%3DRequestProcessor%2Ctype%3Dengine.  
**Monitoring URL:**  /etc/threeshare/system/jcr:content/slingdata.xml  
**Compatibility:**  AEM 6.0+  

#### JVM OS Sensor - *DEPRECATED*
**Usage:**  Used to expose data from OS as determined by the JVM. Status can be seen on AEM in the JMX Operating System page at http://localhost:4502/system/console/jmx/java.lang%3Atype%3DOperatingSystem  
**Monitoring URL:**  /etc/threeshare/system/jcr:content/operatingsystemdata.xml  
**Compatibility:**  AEM 6.0+  


## ROM Tools

#### User List 
**Usage:**  Used to provide a tabular list of all users on the system.  
**URL:**  /etc/threeshare/user-list.html

#### Group List
**Usage:**  Used to provide a tabular list of all groups on the system.  
**URL:**  /etc/threeshare/group-list.html 

#### Header Echo Servlet
**Usage:**  Used to echo all request headers hitting AEM application server. This servlet is not enabled by default and must be enabled in the console config manager under 3|SHARE ROM Header Check Servlet  
**URL:**  /bin/threeshare/headercheck

#### Inventory Query Tool
**Usage:**  This component is used to generate tabular inventory reports along with defined metadata.  
**URL:**  /etc/threeshare/inventory.html?searchPath=/content/dam&assetType=dam:Asset&metadataPath=jcr:content/metadata&metadataFields=dc:title,xmpRights:UsageTerms

#### Querybuilder Cleanup tool
**Usage:**  This tool mimics the usage of the OOB AEM query builder but also has an optional parameter to delete the results of the query by adding parameter delete=true  
**URL:**  /bin/querybuilder.cleanup

#### Workflow Purge Tool
**Usage:**  Workflow purge tool to allow user to crawl workflows and delete slowly. Only useful for extreme numbers of workflows (1,000,000+) where existing purge tools choke due to the number of nodes.  
**URL:**  /etc/threeshare/workflowpurge.html?maxDepth=3&searchPath=/etc/workflow/instances/server0&maxPurgeCount=4&delete=true

## SEO Tools
#### Redirect Manager 
#### Dynamic robots.txt tool 
#### Dynamic sitemap.xml tool

